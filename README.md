# DirectionMoment: a repo/library for computation of direction of object in image by method of the moments

This is a python version of the code found in [this blog(raphael.candelier.fr)](http://www.raphael.candelier.fr/?blog=Image%20Moments) made in Matlab.

This repo gathers codes that are responsible for running the estimation of moments and interpolate an ellipse on the image.  

The `/Conf` folder is responsible for a Configuration with Docker-compose. If DOcker-compose is not installed, please refer to the requirement.txt for the installation. There are too many requirements at the moment (it is sufficient but not necessary). 

`/directionmoment` includes the running code.
`/test` includes unit tests. 
