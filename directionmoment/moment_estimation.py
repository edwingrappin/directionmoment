#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module compute ellipse from moment estimation from an image file
"""

import math
import cv2


def get_moment(img, thresh=100):

    retval, mask_img = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY)
    bin_img = cv2.cvtColor(mask_img, cv2.COLOR_BGR2GRAY)
    res = cv2.moments(bin_img)
    return(res)


def get_ellipse_param(moments):

    m00 = moments["m00"]
    m10 = moments["m10"]
    m01 = moments["m01"]
    m11 = moments["m11"]
    m20 = moments["m20"]
    m02 = moments["m02"]
    bar_x = m01/m00
    bar_y = m10/m00
    nu_01 = m02/m00 - (bar_x ** 2)
    nu_10 = m20/m00 - (bar_y ** 2)
    nu_11 = 2 * (m11/m00 - bar_x * bar_y)
    if_add = (int(nu_01 > nu_10)) * math.pi/2
    theta = 1/2 * math.atan(nu_11/(nu_10 - nu_01)) + if_add

    w = math.sqrt(8*(nu_01+nu_10-math.sqrt(nu_11**2 + (nu_01-nu_10) ** 2)))/2
    lg = math.sqrt(8*(nu_01+nu_10+math.sqrt(nu_11 ** 2+(nu_01-nu_10) ** 2)))/2

    d = math.sqrt(lg ** 2 - w ** 2)
    theta_deg = theta * 180 / math.pi

    return(bar_y, bar_x, w, d, theta_deg)


if __name__ == "__main__":
    dir = "/app/Data/"
    name_file = ["binary_image.png", "binary_image_flip1.png",
                 "binary_image_90.png", "binary_image_90_counter.png"]
    for i_nf in name_file:
        img_path = dir + i_nf
        img = cv2.imread(img_path)
        thresh = 100
        moments = get_moment(img, thresh)
        print(moments)
        bar_y, bar_x, w, d, theta_deg = get_ellipse_param(moments)
        print(get_ellipse_param(moments))
        center_el = (int(bar_y), int(bar_x))
        angle_el = (int(w), int(d))
        theta_inv = theta_deg + 90
        green = (1, 100, 64)
        img_ellipse = cv2.ellipse(img, center_el, angle_el, theta_inv,
                                  0, 360, green, 2)
        cv2.imwrite(dir + "result_" + i_nf, img_ellipse)
