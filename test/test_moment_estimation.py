#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module tests the module moment_estimation.py
"""

import unittest
import cv2

import directionmoment.moment_estimation as dm_moment_est


class TestMomentEstimation(unittest.TestCase):
    moment_input = {'nu12': 6.59448876240989e-05, 'mu03': 19511820389.359863,
                    'mu12': 20055098230.21196, 'm03': 2269163203395.0,
                    'nu30': 5.9953256172134504e-05,
                    'nu21': 6.424120137809767e-05,
                    'm12': 1733496396675.0, 'm10': 67265175.0,
                    'mu21': 19536974744.85887, 'nu11': 0.0018698832520118833,
                    'm01': 90150405.0, 'mu30': 18232928814.84607,
                    'nu02': 0.002088703955063326, 'm00': 621180.0,
                    'm11': 10483559745.0, 'mu02': 805956900.2647781,
                    'nu03': 6.415848918553708e-05, 'm21': 1332970602735.0,
                    'm02': 13889274975.0, 'mu11': 721521738.8731518,
                    'mu20': 689591290.6096058, 'm30': 1030996129125.0,
                    'nu20': 0.0017871328548714122, 'm20': 7973476425.0}

    def test_get_moment(self):
        img_path = "/app/Repo/MomentDirection/test/binary_image.png"
        img = cv2.imread(img_path)
        moment = dm_moment_est.get_moment(img)
        self.assertAlmostEqual(moment, self.moment_input)

    def test_get_ellipse_param(self):
        moment = self.moment_input
        res = dm_moment_est.get_ellipse_param(moment)
        expected_value = (108.28612479474549, 145.1276683087028,
                          12.408331112144824, 96.55276213761981,
                          47.30514981751632)
        self.assertAlmostEqual(res, expected_value)

    def test_image_output(self):
        dir_path = "/app/Repo/MomentDirection/test/"
        name_file = ["binary_image.png", "binary_image_flip1.png",
                     "binary_image_90.png", "binary_image_90_counter.png"]
        for i_nf in name_file:
            img_path = dir_path + i_nf
            img = cv2.imread(img_path)
            thresh = 100
            moments = dm_moment_est.get_moment(img, thresh)
            (bar_y, bar_x, w,
             d, theta_deg) = dm_moment_est.get_ellipse_param(moments)
            center_el = (int(bar_y), int(bar_x))
            angle_el = (int(w), int(d))
            theta_inv = theta_deg + 90
            green = (1, 100, 64)
            img_ellipse = cv2.ellipse(img, center_el, angle_el, theta_inv,
                                      0, 360, green, 2)
            img_compare = cv2.imread(dir_path + "result_" + i_nf)
            self.assertEqual(img_ellipse.shape, img_compare.shape)
            if img_ellipse.shape == img_compare.shape:
                print("The images have same size and channels")
                difference = cv2.subtract(img_ellipse, img_compare)
                b, g, r = cv2.split(difference)
                self.assertEqual(cv2.countNonZero(b), 0)
                self.assertEqual(cv2.countNonZero(g), 0)
                self.assertEqual(cv2.countNonZero(r), 0)
